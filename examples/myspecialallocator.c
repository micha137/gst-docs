/* test compiling this code using:
   gcc -c `pkg-config --cflags glib-2.0 gstreamer-1.0` myspecialallocator.c
 */

#include "myspecialallocator.h"

struct _MySpecialAllocator
{
  GstAllocator parent;

  /* add any required fields here
     [..] */

};

G_DEFINE_TYPE (MySpecialAllocator, my_special_allocator, GST_TYPE_ALLOCATOR)

/* derive a new struct from GstMemory, so we can store additional data */
typedef struct {
  GstMemory mem;
  void* data;
} MyMemory;

static MyMemory* create_my_memory(MySpecialAllocator *allocator, GstMemory *parent,
                                  gsize maxsize, GstMemoryFlags flags,
                                  gsize align, gsize offset, gsize size)
{
  MyMemory* mem = g_slice_alloc(sizeof(MyMemory));
  mem->data = NULL;
  gst_memory_init(GST_MEMORY_CAST(mem), flags, GST_ALLOCATOR_CAST(allocator),
                  parent, maxsize, align, offset, size);
  return mem;
}


static gpointer my_special_allocator_map(GstMemory *memory, gsize maxsize,
    GstMapFlags flags)
{
  /* Mapping can be called multiple times before the next unmap call, so
   * one needs to (ref-)count the map calls in general, and then only do
   * the actual unmapping when the count drops to zero again. Here
   * mapping and unmapping is a no-op, so it doesn't matter in this case.
   */
  MyMemory *mem = (MyMemory *)memory;
  return mem->data;
}

static void my_special_allocator_unmap(GstMemory *memory)
{
}

static GstMemory* my_special_allocator_share(GstMemory *mem, gssize offset,
    gssize size)
{
  GstMemory *parent;

  if ((parent = mem->parent) == NULL)
    parent = mem;
  if (size == -1)
    size = mem->size - offset;

  g_return_val_if_fail (MY_IS_SPECIAL_ALLOCATOR (parent->allocator), NULL);
  MySpecialAllocator *msa = MY_SPECIAL_ALLOCATOR (parent->allocator);

  MyMemory* sub = create_my_memory(msa, parent, mem->maxsize,
      GST_MINI_OBJECT_FLAGS(parent) | GST_MINI_OBJECT_FLAG_LOCK_READONLY,
      mem->align, mem->offset + offset, size);
  sub->data = ((MyMemory*)mem)->data;

  return GST_MEMORY_CAST(sub);
}

static void
my_special_allocator_init (MySpecialAllocator * msa)
{
  GstAllocator *parent = GST_ALLOCATOR(msa);

  /* we implement a custom alloc method */
  GST_OBJECT_FLAG_SET (msa, GST_ALLOCATOR_FLAG_CUSTOM_ALLOC);

  parent->mem_type    = "MyMemory";
  parent->mem_map     = GST_DEBUG_FUNCPTR(my_special_allocator_map);
  parent->mem_unmap   = GST_DEBUG_FUNCPTR(my_special_allocator_unmap);
  parent->mem_share   = GST_DEBUG_FUNCPTR(my_special_allocator_share);

  /* parent->mem_copy and parent->mem_is_span have a default implementation,
   * so they don't need to be impelmented here. Provide them if your custom
   * allocator needs special code to make them work (efficiently).
   */
}

GstMemory * my_special_allocator_alloc (GstAllocator *allocator, gsize size,
                GstAllocationParams *params)
{
  g_return_val_if_fail (MY_IS_SPECIAL_ALLOCATOR (allocator), NULL);
  MySpecialAllocator *msa = MY_SPECIAL_ALLOCATOR (allocator);

  gsize maxsize, offset, alignment, padding;
  MyMemory *mem;

  offset = params->prefix;
  maxsize = size + offset + params->padding;

  /* Now perform the special allocation. */
  mem = create_my_memory(msa, NULL, maxsize, params->flags, params->align, offset, size);
  g_return_val_if_fail (mem, NULL);

  alignment = sizeof(void*);
  while (alignment < params->align)
      alignment *= 2;
  posix_memalign(&mem->data, alignment, maxsize);

  if (offset && (params->flags & GST_MEMORY_FLAG_ZERO_PREFIXED))
      memset(mem->data, 0, offset);

  padding = maxsize - (offset + size);
  if (padding && (params->flags & GST_MEMORY_FLAG_ZERO_PADDED))
      memset(((char*)mem->data) + offset + size, 0, padding);

  return (GstMemory*)mem;
}

static void my_special_allocator_free (GstAllocator *allocator, GstMemory *memory)
{
  g_return_if_fail (MY_IS_SPECIAL_ALLOCATOR (allocator));
  g_return_if_fail (memory);
  MyMemory *mem = (MyMemory *)memory;

  /* now perform the special free. */
  if (memory->parent == NULL)
    free(mem->data);
  g_slice_free1(sizeof(MyMemory), mem);
}

static void my_special_allocator_finalize (GObject * object)
{
  /* nothing special to do in this example */

  /* call parent destructor */
  G_OBJECT_CLASS (my_special_allocator_parent_class)->finalize (object);
}

static void
my_special_allocator_class_init (MySpecialAllocatorClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);
  GstAllocatorClass *allocator_class = GST_ALLOCATOR_CLASS(klass);

  /* we override these two methods of GstAllocator */
  allocator_class->alloc = GST_DEBUG_FUNCPTR (my_special_allocator_alloc);
  allocator_class->free  = GST_DEBUG_FUNCPTR (my_special_allocator_free);

  /* and this method of GObject */
  object_class->finalize = GST_DEBUG_FUNCPTR(my_special_allocator_finalize);
}

/**
 * my_special_allocator_new:
 *
 * Return a new special allocator.
 *
 * Returns: (transfer full): a new special allocator, or NULL if there is not enough memory
 *    available. Use gst_object_unref() to release the allocator after usage
 *
 */
GstAllocator *
my_special_allocator_new (void)
{
  GstAllocator *alloc;

  alloc = g_object_new (MY_TYPE_SPECIAL_ALLOCATOR, NULL);
  gst_object_ref_sink (alloc);

  return alloc;
}

