#include <gst/gst.h>

G_BEGIN_DECLS

#define MY_TYPE_SPECIAL_ALLOCATOR              (my_special_allocator_get_type())
G_DECLARE_FINAL_TYPE(MySpecialAllocator, my_special_allocator, MY, SPECIAL_ALLOCATOR, GstAllocator)

GType my_special_allocator_get_type (void);
GstAllocator * my_special_allocator_new (void);

G_END_DECLS

